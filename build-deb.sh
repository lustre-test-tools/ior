#!/bin/bash

export MPICC="mpicc"
cd ior-*
./bootstrap
./configure  --enable-caps
checkinstall --install=no --pkgname=IOR -y -D make install
