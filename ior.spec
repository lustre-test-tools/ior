Name:           ior
Version:        3.3.0
Release:        0%{?dist}
Summary:        Parallel filesystem I/O benchmark
License:        GPL-2.0
Group:          System/Benchmark
Url:            https://github.com/LLNL/ior
Source:         https://github.com/LLNL/ior/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  mpich-devel
Requires:       mpich
Obsoletes:      mdtest

%description
Parallel filesystem I/O benchmark

%prep
%setup -q

%define debug_package %{nil}

%build
export MPICC=mpicc
%{?_mpich_load}
%if 0%{?rhel} != 8
MPI_BIN=%{_bindir}
%endif
./bootstrap
%_configure --enable-caps --bindir=$MPI_BIN --datadir=/usr/share --libdir=/usr/lib64 --mandir=/usr/share/man
make %{?_smp_mflags}
%{?_mpich_unload}

%install
%make_install
rm %{buildroot}%{_datadir}/USER_GUIDE

%files
%doc COPYRIGHT doc/USER_GUIDE testing

%if 0%{?rhel} == 8
%{_libdir}/mpich/bin/ior
%{_libdir}/mpich/bin/IOR
%{_libdir}/mpich/bin/testlib
%{_libdir}/mpich/bin/mdtest
%else
%{_bindir}/IOR
%{_bindir}/mdtest
%{_libdir}/libaiori.a
%{_bindir}/testlib
%endif

%{_mandir}/man1/mdtest.1.gz

%changelog
