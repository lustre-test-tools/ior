#!/bin/bash

TOPDIR=/tmp/rpmbuild
rm -rf $TOPDIR
rm -f *.rpm
mkdir -p $TOPDIR/SOURCES

tar czf $TOPDIR/SOURCES/ior-3.3.0.tar.gz ior-3.3.0
rpmbuild --define "_topdir $TOPDIR" --define "_bindir /usr/local/bin/" -bs ior.spec

cp $TOPDIR/SRPMS/*.src.rpm .
rpmbuild --define "_topdir $TOPDIR" --define "_unpackaged_files_terminate_build 0" --define "_bindir /usr/local/bin/" --rebuild *.src.rpm
cp $TOPDIR/RPMS/*/* .
